const amqp = require('amqplib/callback_api');
const CronJob = require('cron').CronJob;
const fs = require('fs');

readFile = () => {
    let activities = [];
    let data = fs.readFileSync('activity.txt', 'utf8')
    data = data.split(/\r?\n/);
    for (const line of data) {
        const splitLine = (line.split(/\r?\t/).map(testing => testing.trim())).filter(element => element.length)

        activities.push({
            start: new Date(splitLine[0]).getTime(),
            end: new Date(splitLine[1]).getTime(),
            activity: splitLine[2]
        })
    }

    return activities;
};

amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        let queue = 'DS-queue';

        channel.assertQueue(queue, {
            durable: false
        });

        const fileContent = readFile();

        let lineNo = 0;

        new CronJob('* * * * * *', function() {
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(fileContent[lineNo])));
            console.log(" [x] Sent %s", JSON.stringify(fileContent[lineNo++]));
            if (lineNo === fileContent.length - 1) {
                this.stop();
                connection.close();
                process.exit(0);
            }
        }).start();
    });
});

