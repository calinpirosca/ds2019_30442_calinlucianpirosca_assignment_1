import { user } from "../entities";
import { getRepository, Repository } from "typeorm";

export abstract class UserRepository {
    public static async getUserById(id: number) {
        const userDb = await getRepository(user).findOne(id);
        delete userDb.password;

        return userDb;
    }

    public static async updateUserById(id: number, data: user) {
        const articleRepo: Repository<user> = getRepository(user);

        try {
            await articleRepo.update(id, data);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    public static getPatients() {
        return getRepository(user).find({where: {role: 2}});
    }

    public static getCaregivers() {
        return getRepository(user).find({where: {role: 1}});
    }

    public static async deleteUserById(id: number) {
        try {
            await getRepository(user).delete(id);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    public static async createUser(data: user) {
        const userRepo: Repository<user> = getRepository(user);
        const newUser = userRepo.create(data);

        await userRepo.save(newUser);

        delete newUser.password;

        return newUser;
    }
}