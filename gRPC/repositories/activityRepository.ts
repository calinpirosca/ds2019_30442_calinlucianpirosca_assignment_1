import {activity} from "../entities";
import {getRepository} from "typeorm";

export abstract class ActivityRepository {
    public static async CreateActivity(data: {userId: number, start: string, end: string, activity: string}): Promise<activity> {
        const newActivity = getRepository(activity).create(data);

        await getRepository(activity).save(newActivity);

        return newActivity;
    }
}