import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {medication} from "./medication";
import {side_effects} from "./side_effects";


@Entity("medication_side_effects" ,{schema:"ds_medical" } )
@Index("id_medication_idx",["idMedication",])
@Index("id_side_effect_idx",["idSideEffects",])
export class medication_side_effects {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(()=>medication, (medication: medication)=>medication.medicationSideEffectss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_medication'})
    idMedication:medication | null;


   
    @ManyToOne(()=>side_effects, (side_effects: side_effects)=>side_effects.medicationSideEffectss,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_side_effects'})
    idSideEffects:side_effects | null;

}
