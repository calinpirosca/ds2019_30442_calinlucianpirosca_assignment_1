import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {medication_side_effects} from "./medication_side_effects";


@Entity("side_effects" ,{schema:"ds_medical" } )
export class side_effects {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"description"
        })
    description:string;
        

   
    @OneToMany(()=>medication_side_effects, (medication_side_effects: medication_side_effects)=>medication_side_effects.idSideEffects,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    medicationSideEffectss:medication_side_effects[];
    
}
