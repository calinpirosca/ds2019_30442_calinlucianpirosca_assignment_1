import { user } from "../entities";
import {UserRepository} from "../repositories/userRepository";

export abstract class UserService {
    public static getUserById(id: number) {
        return UserRepository.getUserById(id);
    }

    public static updateUserById(id: number, data: user) {
        return UserRepository.updateUserById(id, data);
    }

    public static deleteUserById(id: number) {
        return UserRepository.deleteUserById(id);
    }

    public static getPatients() {
        return UserRepository.getPatients();
    }

    public static getCaregivers() {
        return UserRepository.getCaregivers();
    }
}