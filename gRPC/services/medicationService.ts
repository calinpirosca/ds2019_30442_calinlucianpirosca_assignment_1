import {caregiver_patient, medical_record, medication, medication_side_effects} from "../entities";
import { MedicationRepository } from "../repositories";

export abstract class MedicationService {
    public static getMedicationById(id: number) {
        return MedicationRepository.getMedicationById(id);
    }

    public static getAllMedication() {
        return MedicationRepository.getAllMedication();
    }

    public static addEffectToMedication(data: medication_side_effects) {
        return MedicationRepository.addEffectToMedication(data);
    }

    public static getAllSideEffects() {
        return MedicationRepository.getAllSideEffects();
    }

    public static addCaregiverToPatient(data: caregiver_patient) {
        return MedicationRepository.addCaregiverToPatient(data);
    }

    public static updateMedicationById(id: number, data: medication) {
        return MedicationRepository.updateMedicationById(id, data);
    }

    public static getAllPlansByPatientId(id: number) {
        return MedicationRepository.getAllPlansByPatientId(id);
    }

    public static getSideEffectsByMedicationId(id: number) {
        return MedicationRepository.getSideEffectsByMedicationId(id);
    }

    public static getPatientsAndPlansByCaregiver(id: number) {
        return MedicationRepository.getPatientsAndPlansByCaregiver(id);
    }

    public static deleteMedicationById(id: number) {
        return MedicationRepository.deleteMedicationById(id);
    }

    public static async createMedication(data: medication) {
        return MedicationRepository.createMedication(data);
    }

    public static createMedicationPlan(data: medical_record) {
        return MedicationRepository.createMedicationPlan(data);
    }
}