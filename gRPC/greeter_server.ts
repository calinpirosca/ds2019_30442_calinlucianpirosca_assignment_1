/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { createConnection, Connection } from 'typeorm';
import {MedicationService} from "./services";

const connection = createConnection();

let PROTO_PATH = './helloworld.proto';

let grpc = require('grpc');
let protoLoader = require('@grpc/proto-loader');
let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
let hello_proto = grpc.loadPackageDefinition(packageDefinition).helloworld;

/**
 * Implements the getAllPlansByPatientId RPC method.
 */
async function getAllPlansByPatientId(call, callback) {
    // use cron job
    const data = await MedicationService.getAllPlansByPatientId(call.request.id);
    callback(null, {medication_plan: data});
    console.log('Reached gRPC server -- getAllPlansByPatientId');
}

async function handleMedicationTaken(call, callback) {
    callback(null, {medication_plan: null});
    console.log('Reached gRPC server -- handleMedicationTaken', call.request.id);
}

async function handleMedicationNotTaken(call, callback) {
    callback(null, {medication_plan: null});
    console.log('Reached gRPC server -- handleMedicationNotTaken', call.request.id);
}



/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */
function main() {
    connection
        .then(() => {
            let server = new grpc.Server();
            server.addService(hello_proto.MedicalPlan.service, {
                getAllPlansByPatientId: getAllPlansByPatientId,
                handleMedicationTaken: handleMedicationTaken,
                handleMedicationNotTaken: handleMedicationNotTaken
            });
            server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
            server.start();
        })
        .catch(console.error);

}

main();
