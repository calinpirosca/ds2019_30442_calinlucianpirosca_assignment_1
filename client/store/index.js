import { decode } from 'jsonwebtoken'
import Cookie from 'js-cookie'
import CookieParser from 'cookieparser'

export const state = () => ({
  token: null,
  user: null
});

export const mutations = {
  login (state, data) {
    state.token = data.token;
    state.user = decode(data.token);
  },

  logout (state) {
    state.token = null;
    state.user = null;
  }
};

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    let auth;
    if (req && req.headers.cookie) {
      const parsed = CookieParser.parse(req.headers.cookie);
      try {
        if (parsed.token) {
          auth = JSON.parse(parsed.token);
          commit('login', auth);
        }
      } catch (e) {
        Cookie.remove('token');
        console.warn('Could not parse auth token');
      }
    }
  },
};
