export default function ({store, redirect}) {
  if (!store.state.token || store.state.user.role !== 2) {
    redirect('/');
  }
}
