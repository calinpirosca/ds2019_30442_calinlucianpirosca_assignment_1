export default function ({store, redirect}) {
  if (store.state.token) {
    switch(store.state.user.role){
      case 0:
        redirect('/doctor');
        break;
      case 1:
        redirect('/caregiver');
        break;
      case 2:
        redirect('/patient');
        break;
    }
  }
}
