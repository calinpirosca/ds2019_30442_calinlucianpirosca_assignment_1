import { Body, Post, JsonController } from 'routing-controllers';
import {AuthService} from "../services";
import {client} from "../grpc_server";

@JsonController('/auth')
export class AuthController {

    /**
     * login
     * @param user
     */
    @Post("/")
    async login(@Body() user: {email: string, password: string}) {
        try {
            return await AuthService.login(user);
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}