import {Param, Body, Get, Post, JsonController, Authorized, Patch, Delete, Req} from 'routing-controllers';
import {roles, secret} from '../utils';
import { MedicationService } from "../services";
import {caregiver_patient, medical_record, medication, medication_side_effects} from "../entities";
import {verify} from "jsonwebtoken";
import {client} from "../grpc_server";

@JsonController('/medication')
export class MedicationController {

    @Authorized(roles.DOCTOR)
    @Post("/")
    CreateMedication(@Body() medication: medication) {
        return MedicationService.createMedication(medication);
    }

    @Authorized(roles.DOCTOR)
    @Post("/plan")
    createMedicationPlan(@Body() medicationPlan: medical_record) {
        return MedicationService.createMedicationPlan(medicationPlan);
    }

    @Authorized(roles.PATIENT)
    @Get("/patient/plan")
    getMedicationPlan(@Req() request: any) {
        const decodedToken = verify(request.header.authorization.substring(7), secret) as {id: number};

        return getAllPlansByPatientId(decodedToken.id);

        // return MedicationService.getAllPlansByPatientId(decodedToken.id);
    }

    @Authorized([roles.DOCTOR, roles.CAREGIVER])
    @Get("/patient/plan/:id")
    getMedicationPlanByPatientId(@Param("id") id:number) {
        return MedicationService.getAllPlansByPatientId(id);
    }

    @Authorized()
    @Get("/getAll/sideeffects")
    getAllSideEffects(){
        return MedicationService.getAllSideEffects();
    }

    @Authorized()
    @Get("/sideeffects/:id")
    getSideEffectsByMedicationId(@Param("id") id:number){
        return MedicationService.getSideEffectsByMedicationId(id);
    }

    @Authorized()
    @Get("/medicationtaken/:medical_plan_id")
    handleMedicationTaken(@Param("medical_plan_id") id:number){
        client.handleMedicationTaken({id: id}, function(err, response) {
            // console.log('Greeting:', response.message);
            console.log(response);
            console.log('Reached gRPC client', id);
        });

        return true;
    }

    @Authorized()
    @Get("/medicationnottaken/:medical_plan_id")
    handleMedicationNotTaken(@Param("medical_plan_id") id:number){
        console.log(id);
        client.handleMedicationNotTaken({id: id}, function(err, response) {
            // console.log('Greeting:', response.message);
            console.log(response);
            console.log('Reached gRPC client', id);
        });

        return true;
    }

    @Authorized()
    @Post("/sideeffects")
    addSideEffectToMedication(@Body() data: medication_side_effects) {
        return MedicationService.addEffectToMedication(data);
    }


    @Authorized(roles.CAREGIVER)
    @Get("/caregiver/patients")
    getPatientsAndPlans(@Req() request: any) {
        const decodedToken = verify(request.header.authorization.substring(7), secret) as {id: number};
        return MedicationService.getPatientsAndPlansByCaregiver(decodedToken.id);
    }

    @Authorized(roles.DOCTOR)
    @Post("/caregiver/patients")
    addCaregiverToPatient(@Body() data: caregiver_patient) {
        return MedicationService.addCaregiverToPatient(data);
    }


    @Authorized(roles.DOCTOR)
    @Get("/caregiver/patients/:caregiverId")
    getPatientsAndPlansOfCaregiver(@Param("caregiverId") caregiverId: number) {
        return MedicationService.getPatientsAndPlansByCaregiver(caregiverId);
    }

    @Authorized(roles.DOCTOR)
    @Get("/")
    getAll() {
        return MedicationService.getAllMedication();
    }

    @Authorized(roles.DOCTOR)
    @Get("/:id")
    getOne(@Param("id") id: number) {
        return MedicationService.getMedicationById(id);
    }

    @Authorized(roles.DOCTOR)
    @Patch("/:id")
    updateMedication(@Param("id") id: number, @Body() data: medication) {
        return MedicationService.updateMedicationById(id, data);
    }

    @Authorized(roles.DOCTOR)
    @Delete("/:id")
    deleteMedication(@Param("id") id:number) {
        return MedicationService.deleteMedicationById(id);
    }
}

const getAllPlansByPatientId = async (id) => {
    const wrapCallback = new Promise((resolve, reject) => {
            client.getAllPlansByPatientId({id: id}, function(err, response) {
                // console.log('Greeting:', response.message);
                console.log(response);
                console.log('Reached gRPC client', id);

                resolve(response.medication_plan);
            });
    })

    return await wrapCallback;
};