import { Ctx, Get, JsonController } from 'routing-controllers';

@JsonController('/ws')
export class WsController {

    @Get("/")
    async get(@Ctx() ctx: any) {
        console.log('hereeee', ctx.websocket);
        return 'Success';
    }
}