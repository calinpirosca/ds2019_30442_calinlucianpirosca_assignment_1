import {Param, Body, Get, Post, JsonController, Authorized, Patch, Delete} from 'routing-controllers';
import { roles } from '../utils';
import { user } from "../entities";
import { UserService } from "../services";

@JsonController('/users')
export class UserController {

    @Authorized(roles.DOCTOR)
    @Post("/")
    CreateUser(@Body() user: user) {
        return UserService.createUser(user);
    }

    @Authorized(roles.DOCTOR)
    @Get("/byId/:id")
    getOne(@Param("id") id: number) {
        return UserService.getUserById(id);
    }

    @Authorized(roles.DOCTOR)
    @Get("/patients")
    getPatients() {
        return UserService.getPatients();
    }

    @Authorized(roles.DOCTOR)
    @Get("/caregivers")
    getCaregivers() {
        return UserService.getCaregivers();
    }

    @Authorized(roles.DOCTOR)
    @Patch("/:id")
    updateUser(@Param("id") id: number, @Body() data: user) {
        return UserService.updateUserById(id, data);
    }

    @Authorized(roles.DOCTOR)
    @Delete("/:id")
    deleteUser(@Param("id") id:number) {
        return UserService.deleteUserById(id);
    }
}