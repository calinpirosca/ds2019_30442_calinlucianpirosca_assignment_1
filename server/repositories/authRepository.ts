import { user } from "../entities";
import { getRepository } from "typeorm";

export abstract class AuthRepository {
    public static getUserByEmail(email: string): Promise<user> {
        return getRepository(user).findOne({where: { email }});
    }
}