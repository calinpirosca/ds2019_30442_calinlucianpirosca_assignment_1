import {caregiver_patient, medical_record, medication, medication_side_effects, side_effects} from "../entities";
import {createQueryBuilder, getManager, getRepository, Repository} from "typeorm";

export abstract class MedicationRepository {
    public static getMedicationById(id: number) {
        return getRepository(medication).findOne(id);
    }

    public static getAllMedication() {
        return getRepository(medication).find();
    }
    public static getAllSideEffects() {
        return getRepository(side_effects).find({});
    }

    public static async getAllPlansByPatientId(id: number) {
        return createQueryBuilder(medical_record)
            .innerJoinAndSelect('user', 'doctor', 'doctor.id = medical_record.id_doctor')
            .innerJoinAndSelect('medication', 'medication', 'medication.id = medical_record.id_medication')
            .where(`id_patient = ${id}`)
            .getRawMany();


    }

    public static getSideEffectsByMedicationId(id: number) {
        return createQueryBuilder(medication_side_effects)
            .innerJoinAndSelect('side_effects', 'side_effects', `side_effects.id = medication_side_effects.id_side_effects`)
            .where(`id_medication = ${id}`)
            .select([
                'side_effects.description as description'
            ])
            .getRawMany();
    }

    public static async getPatientsAndPlansByCaregiver(id: number): Promise<any> {
        const caregiverPatient = await createQueryBuilder(caregiver_patient)
            .innerJoinAndSelect('user', 'patient', 'patient.id = id_patient')
            .where(`id_caregiver = ${id}`)
            .getRawMany()
        for(const cp of caregiverPatient) {
            const medication = await createQueryBuilder(medical_record)
                .innerJoinAndSelect('user', 'doctor', 'doctor.id = medical_record.id_doctor')
                .innerJoinAndSelect('medication', 'medication', 'medication.id = medical_record.id_medication')
                .where(`medical_record.id_patient = ${cp.patient_id}`)
                .getRawMany();
            for (const med of medication) {
                const sideEffects = await this.getSideEffectsByMedicationId(med.medication_id);
                Object.assign(med, {side_effects: sideEffects});
            }
            Object.assign(cp, {medication: medication});
        }

        return caregiverPatient;
    }

    public static async updateMedicationById(id: number, data: medication) {
        const medicationRepo: Repository<medication> = getRepository(medication);

        try {
            await medicationRepo.update(id, data);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    public static async deleteMedicationById(id: number) {
        try {
            await getRepository(medication).delete(id);
            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    public static async createMedication(data: medication) {
        const medicalRepo: Repository<medication> = getRepository(medication);
        const newMedication = medicalRepo.create(data);

        await medicalRepo.save(newMedication);

        return newMedication;
    }

    public static async addEffectToMedication(data: medication_side_effects) {
        const medicalRepo: Repository<medication_side_effects> = getRepository(medication_side_effects);
        const newMedication = medicalRepo.create(data);

        await medicalRepo.save(newMedication);

        return newMedication;
    }

    public static async addCaregiverToPatient(data: caregiver_patient) {
        const medicalRepo: Repository<caregiver_patient> = getRepository(caregiver_patient);
        const newMedication = medicalRepo.create(data);

        await medicalRepo.save(newMedication);

        return newMedication;
    }

    public static async createMedicationPlan(data: medical_record) {
        const medicationPlanRepo: Repository<medical_record> = getRepository(medical_record);

        const newMedicalPlan = medicationPlanRepo.create(data);

        await medicationPlanRepo.save(newMedicalPlan);

        return newMedicalPlan;
    }
}