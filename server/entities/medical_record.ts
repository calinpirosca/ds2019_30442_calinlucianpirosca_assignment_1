import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {user} from "./user";
import {medication} from "./medication";


@Entity("medical_record" ,{schema:"ds_medical" } )
@Index("doctor_idx",["idDoctor",])
@Index("medication_idx",["idMedication",])
@Index("patient_idx",["idPatient",])
export class medical_record {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(()=>user, (user: user)=>user.medicalRecords2,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_patient'})
    idPatient:user | null;


    @Column("date",{ 
        nullable:false,
        name:"start_treatment"
        })
    start_treatment:string;
        

    @Column("date",{ 
        nullable:false,
        name:"end_treatment"
        })
    end_treatment:string;
        

   
    @ManyToOne(()=>user, (user: user)=>user.medicalRecords,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_doctor'})
    idDoctor:user | null;


   
    @ManyToOne(()=>medication, (medication: medication)=>medication.medicalRecords,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_medication'})
    idMedication:medication | null;


    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"dosage"
        })
    dosage:string;
        
}
