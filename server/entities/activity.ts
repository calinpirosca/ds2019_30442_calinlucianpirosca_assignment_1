import {Column,Entity,Index,JoinColumn,ManyToOne,PrimaryGeneratedColumn} from "typeorm";
import {user} from "./user";


@Entity("activity" ,{schema:"ds_medical" } )
@Index("userId_idx",["user",])
export class activity {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(()=>user, (user: user)=>user.activitys,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'userId'})
    user:user | null;


    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"start"
        })
    start:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"end"
        })
    end:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"activity"
        })
    activity:string;
        
}
