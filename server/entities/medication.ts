import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {medication_side_effects} from "./medication_side_effects";
import {medical_record} from "./medical_record";


@Entity("medication" ,{schema:"ds_medical" } )
@Index("id_UNIQUE",["id",],{unique:true})
export class medication {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"name"
        })
    name:string;
        

   
    @OneToMany(()=>medication_side_effects, (medication_side_effects: medication_side_effects)=>medication_side_effects.idMedication,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    medicationSideEffectss:medication_side_effects[];
    

   
    @OneToMany(()=>medical_record, (medical_record: medical_record)=>medical_record.idMedication,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    medicalRecords:medical_record[];
    
}
