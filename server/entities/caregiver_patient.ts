import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {user} from "./user";


@Entity("caregiver_patient" ,{schema:"ds_medical" } )
@Index("id_caregiver_idx",["idCaregiver",])
@Index("id_patient_idx",["idPatient",])
export class caregiver_patient {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

   
    @ManyToOne(()=>user, (user: user)=>user.caregiverPatients,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_caregiver'})
    idCaregiver:user | null;


   
    @ManyToOne(()=>user, (user: user)=>user.caregiverPatients2,{  nullable:false,onDelete: 'CASCADE',onUpdate: 'NO ACTION' })
    @JoinColumn({ name:'id_patient'})
    idPatient:user | null;

}
