import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {medical_record} from "./medical_record";
import {caregiver_patient} from "./caregiver_patient";
import {activity} from "./activity";


@Entity("user" ,{schema:"ds_medical" } )
@Index("email_UNIQUE",["email",],{unique:true})
export class user {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"id"
        })
    id:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"name"
        })
    name:string;
        

    @Column("date",{ 
        nullable:false,
        name:"birth_date"
        })
    birth_date:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"gender"
        })
    gender:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:45,
        name:"address"
        })
    address:string;
        

    @Column("int",{ 
        nullable:false,
        name:"role"
        })
    role:number;
        

    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:45,
        name:"email"
        })
    email:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"password"
        })
    password:string;


    @OneToMany(()=>activity, (activity: activity)=>activity.user,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    activitys:activity[];
   
    @OneToMany(()=>medical_record, (medical_record: medical_record)=>medical_record.idDoctor,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    medicalRecords:medical_record[];
    

   
    @OneToMany(()=>caregiver_patient, (caregiver_patient: caregiver_patient)=>caregiver_patient.idCaregiver,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    caregiverPatients:caregiver_patient[];
    

   
    @OneToMany(()=>caregiver_patient, (caregiver_patient: caregiver_patient)=>caregiver_patient.idPatient,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    caregiverPatients2:caregiver_patient[];
    

   
    @OneToMany(()=>medical_record, (medical_record: medical_record)=>medical_record.idPatient,{ onDelete: 'CASCADE' ,onUpdate: 'NO ACTION' })
    medicalRecords2:medical_record[];
    
}
