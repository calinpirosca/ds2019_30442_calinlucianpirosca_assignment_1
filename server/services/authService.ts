import { AuthRepository } from "../repositories";
import { compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { secret } from '../utils';

export abstract class AuthService {

    public static async login(data: {email: string, password: string}) {
        const user = await AuthRepository.getUserByEmail(data.email);

        if (!user) {
            throw "Incorrect credentials";
        }

        const comparedPasswords = await compare(data.password, user.password);

        if (!comparedPasswords) {
            throw "Incorrect credentials";
        }

        delete user.password;

        const token = sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            ...user
        }, secret);

        return {token: token};
    }
}