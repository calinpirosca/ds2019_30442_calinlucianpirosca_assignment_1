import { user } from "../entities";
import {UserRepository} from "../repositories";
import { hash } from 'bcrypt';

export abstract class UserService {
    public static getUserById(id: number) {
        return UserRepository.getUserById(id);
    }

    public static updateUserById(id: number, data: user) {
        return UserRepository.updateUserById(id, data);
    }

    public static deleteUserById(id: number) {
        return UserRepository.deleteUserById(id);
    }

    public static getPatients() {
        return UserRepository.getPatients();
    }

    public static getCaregivers() {
        return UserRepository.getCaregivers();
    }

    public static async createUser(data: user) {
        try {
            data.password = await hash(data.password, 10);
            return UserRepository.createUser(data);
        } catch (err) {
            console.log(err);
        }
    }
}