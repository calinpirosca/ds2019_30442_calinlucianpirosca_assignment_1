import {AuthRepository, MedicationRepository} from "../repositories";
import {ActivityRepository} from "../repositories/activityRepository";
import {rules} from "../utils";
import {getSockets} from "../index";

var amqp = require('amqplib/callback_api');

export const startConsumer = () => {
    amqp.connect('amqp://localhost', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1;
            }
            let queue = 'DS-queue';

            channel.assertQueue(queue, {
                durable: false
            });

            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
            channel.consume(queue, async function(msg) {
                const activity = JSON.parse(msg.content.toString());
                console.log(" [x] Received", activity);
                try {
                    const connections = getSockets().filter(connection => connection.user.role == 1);
                    const caregiver = (connections[0] || {}).user;


                    const patient = await AuthRepository.getUserByEmail('patient1');
                    await ActivityRepository.CreateActivity({user: patient.id, ...activity});

                    let patients;
                    if (caregiver) {
                        patients = await MedicationRepository.getPatientsAndPlansByCaregiver(caregiver.id);
                        patients = patients.filter(pat => pat.caregiver_patient_id_patient == patient.id);
                    }
                    if (rules[activity.activity] < ((activity.end - activity.start) / 1000 / 60)) {
                        if (patients.length) {
                            connections[0].socket.send(`User ${patient.id} is dying ! ${JSON.stringify(activity)}`);
                        }
                        console.log('WARNING, ACTIVITY %s TOOK TOO LONG', activity.activity)
                    }
                } catch (err) {
                    console.log(err);
                }
            }, {
                noAck: true
            });
        });
    });
}

