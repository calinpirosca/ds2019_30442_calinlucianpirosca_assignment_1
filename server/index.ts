import "reflect-metadata";
import * as Koa from 'koa';
import * as cors from '@koa/cors';
import * as bodyParser from 'koa-bodyparser';
import * as Logger from 'koa-logger';
import * as websockify from 'koa-websocket';
import * as route from 'koa-route';
import { verify } from 'jsonwebtoken';
import { useKoaServer, Action } from 'routing-controllers';
import {AuthController, MedicationController, UserController, WsController} from './controllers';
import { createConnection, Connection } from 'typeorm';
import {secret} from "./utils";
import {startConsumer} from "./sensors/consumer";
import {client, grpc_client} from "./grpc_server";



// This uses the settings inside ormconfig.js
const connection: Promise<Connection> = createConnection();

export const server: Koa = new Koa();
export const app = websockify(server);

const port = 3000;

// Generic error handling middleware
server.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (error) {
        console.log(error);
        ctx.app.emit('Error caught', error, ctx);
    }
});

server.use(bodyParser());

server.use(Logger());

server.use(cors({
    origin: "*",
    allowHeaders: "*",
    keepHeadersOnError: true,
    maxAge: 3600,
    allowMethods: "GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS",
}));

useKoaServer(server, {
    controllers: [UserController, AuthController, MedicationController, WsController],
    authorizationChecker: async (action: Action, roles: string[]) => {
        const token = action.request.headers["authorization"];

        if (!token) {
            return false;
        }

        const decodedToken = verify((token as string).substring(7), secret) as {exp: number, role: number};

        if (decodedToken) {
            if (decodedToken.exp < parseInt(Date.now().toString().slice(0, -3))) {
                console.log("Expired token");
                return false;
            }
            if (roles.includes(decodedToken.role.toString()) || !roles.length) {
                return true;
            } else {
                console.log("Invalid role");
                return false;
            }
        } else {
            console.log("Invalidt JWT");
            return false;
        }
    }
});

connection
    .then(() => server.listen(port,() => {
        console.log(`Server listening on ${port}`);
        startConsumer();
        startWebsocket();
        grpc_client();
    }))
    .catch(console.error);

let websockets = [];

export const getSockets = () => {
    return websockets;
};

const startWebsocket = () => {
    app.ws.use(route.all('/ws', (ctx) => {
        ctx.websocket.send('Welcome, looser');
        // the websocket is added to the context as `ctx.websocket`.
        ctx.websocket.on('message', function(message) {
            // print message from the client
            try {
                message = JSON.parse(message);
                const decodedToken = verify(message.token, secret);
                websockets.push({socket: ctx.websocket, user: decodedToken});
            }catch(err) {

            }
            for (const websocket of websockets) {
                if(websocket.socket != ctx.websocket) {
                    websocket.socket.send('Someone just connected !');
                }
            }
        });


        ctx.websocket.on('close', function (test) {
            console.log("Client left.", test);

            // Todo to be updated considering the new object structure (socket: {}, user:{} )
            let index = websockets.indexOf(ctx.websocket);
            websockets.splice(index, 1);
        });
    }));
};
